## Klausur Test 3 des 18.03.

* evtl. Fehler finden
* Logik ausprogrammieren (z.B. if/else)
* Fehler behandeln (wenn der Fehler so und so ist, dann gebe dem Kunden folgendes zurück)
* String verketten (SQL-Statement)
* if/else (auch verschachtelt)
* zur SQL-Syntax eine Aufgabe 
* Übergabe einer Variablen an ein SQL-Statement
* app.post und app.get (auseinanderhalten, programmieren können)
* evtl. Modul einbinden, das etwas macht, z.B. "email-validator" (so wie das iban Modul, wie man das anwendet, externes Modul einbinden und einbauen)